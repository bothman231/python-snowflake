#!/usr/bin/env python
from sqlalchemy import create_engine
import os

engine = create_engine(
    'snowflake://{user}:{password}@{account}/{db}/{schema}?warehouse={warehouse}&authenticator={authenticator}'.format(
        user=os.environ['SF_USER'],
        password=os.environ['SF_PASSWORD'],
        account=os.environ['SF_ACCOUNT'],
        db=os.environ['SF_DB'],
        schema=os.environ['SF_SCHEMA'],
        warehouse=os.environ['SF_WAREHOUSE'],
        role=os.environ['SF_ROLE'],
        authenticator=os.environ['SF_AUTHENTICATOR']
    )
)
try:
    connection = engine.connect()
    results = connection.execute('select current_version()').fetchone()
    print(results[0])
finally:
    connection.close()
    engine.dispose()
