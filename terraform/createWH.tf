provider "snowflake" {
   region     = "us-east-1"
}


resource "snowflake_warehouse" "dev_wh" {
      name              =   "dev_wh"
      warehouse_size    =   "SMALL"
      auto_resume       =   false
      auto_suspend      =   600
      comment           =   "terraform development warehouse"
}
