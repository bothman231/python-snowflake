from os import system, name 
import os

class Subscription:

  def __init__(self, acctId, acctName):
    self.acctId = acctId
    self.acctName = acctName

# Cant have multiple constructors
# def __init__():
#   self.acctId = "1111"
#   self.acctName="Test name"


  def __str__(self):
      return "acctId="+self.acctId+ \
             " acctName="+self.acctName


