#!/usr/bin/env python
import snowflake.connector

SCHEMA="TPCH_SF001"
TABLE="CUSTOMER"

# Gets the version
ctx = snowflake.connector.connect(
    user='sbotham',
    password='Password29!',
    account='arc.us-east-1',
    authenticator= 'https://arccorp.okta.com/',
    database='SNOWFLAKE_SAMPLE_DB',
    schema=SCHEMA,
#   role='PUBLIC',
    role='FNCRL_D_TIPSTERS',
#   role='SCHRL_DEV_WH_DB01_D_BI_TIP_TBLS_FULLACCESS',
    warehouse='NP_WH_XS_TIPSTERS'
)

def getVersion():
   cs = ctx.cursor()
   try:
     cs.execute("SELECT current_version()")
     one_row = cs.fetchone()
     print("Version="+one_row[0])
   finally:
     cs.close()


def getCount():
   cur = ctx.cursor()
   try:
      cur.execute("SELECT count(*) from "+TABLE)   
      for (col1) in cur:
         print("Count="+'{0}'.format(col1))
   finally:
     cur.close()
   return

def getAll(): 
   WHERE=" WHERE customer.c_name='Customer#000000003' and customer.C_phone='11-719-748-3364'"
#  WHERE=""
   LIMIT=" LIMIT 5"
   cur = ctx.cursor()
   try:
     SELECT="SELECT customer.c_name, customer.c_phone from customer";
     FULL=SELECT+WHERE+LIMIT
     cur.execute(FULL)
     for (col1) in cur:
        print("Customers="+'{0}'.format(col1))
   finally:
     cur.close()
   return


def insert():
   cur = ctx.cursor()
   try:
     INSERT="INSERT INTO "+TABLE+" ( C_NAME ) VALUES ('C_NAME VALUE')"
     FULL=INSERT
     cur.execute(FULL)
     for (col1) in cur:
        print('{0}'.format(col1))
   finally:
     cur.close()
   return

def delete():
   cur = ctx.cursor()
   try:
     DELETE="DELETE FROM CUSTOMER WHERE customer.c_name='XYZ'"
     FULL=DELETE
     cur.execute(FULL)
     for (col1) in cur:
        print('{0}'.format(col1))
   finally:
     cur.close()
   return

def update():
   WHERE=" WHERE customer.c_name='Customer#000000003' and customer.C_phone='11-719-748-3364'"
   cur = ctx.cursor()
   try:
     UPDATE="UPDATE CUSTOMER SET customer.c_name='XYZ'"
     FULL=UPDATE+WHERE
     cur.execute(FULL)
     for (col1) in cur:
        print('{0}'.format(col1))
   finally:
     cur.close()
   return


getVersion();
#getCount();
#insert();
#delete();
#update();
getAll();

ctx.close();
